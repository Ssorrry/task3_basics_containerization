FROM python:3.9-slim

WORKDIR /app

COPY . .

CMD ["python", "Task2_NumberSearch/task2_number_search.py", "$SEARCH_VALUE"]