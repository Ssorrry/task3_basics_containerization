"""
Модуль для выполнения бинарного поиска в отсортированном массиве.
"""

import random
import sys
import os

def generate_sorted_array(size):
    """
    Генерирует отсортированный массив случайных чисел.

    :param size: Размер массива.
    :return: Отсортированный массив случайных чисел.
    """
    return sorted([random.randint(1, 1000) for _ in range(size)])

def binary_search(arr, tgt):
    """
    Выполняет бинарный поиск в отсортированном массиве.

    :param arr: Отсортированный массив.
    :param tgt: Искомое значение.
    :return: Индекс искомого значения в массиве или -1, если значение не найдено.
    """
    left, right = 0, len(arr) - 1
    while left <= right:
        mid = (left + right) // 2
        if arr[mid] == tgt:
            return mid
        if arr[mid] < tgt:
            left = mid + 1
        else:
            right = mid - 1
    return -1

if __name__ == "__main__":
    search_value = os.environ.get('SEARCH_VALUE')
    try:
        target = int(search_value)
    except ValueError:
        print("Пожалуйста, введите целое число.")
        sys.exit(1)

    array = generate_sorted_array(100)
    print("Сгенерированный массив:", array)

    index = binary_search(array, target)
    if index != -1:
        print(f"Искомое значение {target} найдено на позиции {index}.")
    else:
        print(f"Искомое значение {target} не найдено в массиве.")
        